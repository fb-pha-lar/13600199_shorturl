
<link href="https://fonts.googleapis.com/css?family=Luckiest+Guy|Mitr|Monoton|Righteous|Yanone+Kaffeesatz&display=swap" rel="stylesheet">
<body style="background-color: whitesmoke; ">
@extends('layouts.app')
@section('content')
<div class="container card" style="background-color: white">

    <br>
    <form method="get" action="{{ url('/new') }}">
        <button type="submit" class="btn btn-lg btn-block" style="background-color: darkorchid; color: white">GO TO SHORT URL</button>
    </form>
    <form>
        <br>
        <div class="card text-center" style="background-color: indigo">
            <div class="card-header">
                <h1  style=" height: 150px; color: white; padding-top: 50px; font-size: 50px; font-family: 'Monoton' ;">LIST URL</h1>
            </div>
        </div>
        <br>
        @if(count($shortens)>0)
            @foreach($shortens as $shorten)
                <div style="font-family: 'Righteous'">
                    <a href="{{ url($shorten -> long) }}">
                        <h3 class="text" style="color: darkslateblue">{{ $shorten -> long }} </h3>
                    </a>
                    <p>DATE : {{ $shorten -> created_at }}</p>
                    <div class="input-group mb-3" style="font-family: 'Yanone Kaffeesatz'">
                        <div class="input-group-prepend">
                            <p class="input-group-text" style="background-color: darkslateblue; color: white">View : {{ $shorten -> view }}</p>
                        </div>
                        <input id="shorturl{{ $shorten -> id }}" class="form-control" type="text" style="background-color: white" value="http://www.short.local/t/{{ $shorten -> short}}" readonly>
                        <div class="input-group-prepend">
                            <button onclick="copy(this)" id="copyBtn" value="{{ $shorten -> id }}" type="button" class="btn" style="background-color: slateblue; color: white">COPY</button>
                        </div>
                    </div>
                    <hr class="my-4">
                </div>
            @endforeach
        @endif
    </form>

</div>
@endsection


<script>
    function copy(clickedBtn) {
        var id = clickedBtn.value;
        var copyText = document.querySelector('#shorturl'+id)
        copyText.select();
        document.execCommand('copy');
        alert('Copied' + copyText.value);
    }
</script>


</body>


