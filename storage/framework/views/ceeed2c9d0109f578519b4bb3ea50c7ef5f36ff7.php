<link href="https://fonts.googleapis.com/css?family=Luckiest+Guy|Mitr|Monoton|Righteous|Yanone+Kaffeesatz&display=swap" rel="stylesheet">
<body style="background-color: whitesmoke; ">

<?php $__env->startSection('content'); ?>
    <div class="container card" style="background-color: white">
        <br>
    <form>
        <div class="jumbotron " style="font-family: 'Monoton' ; background-color: firebrick ; color: white">
            <h1 class="display-4">NOT FOUND</h1>
            <br>
            <p class="lead" style="font-family: 'Mitr' ;">“ไม่พบรหัส SHORT URL”</p>
            <hr class="my-4" style="background-color: white">
            <p style="font-family: 'Mitr' ;">โปรดลองใหม่อีกครั้งที่ http://www.short.local/</p>
        </div>
    </form>
    <form method="get" action="<?php echo e(url('/')); ?>" >
        <button type="submit" class="btn btn-lg btn-block" style="background-color: crimson; color: white">BACK TO LIST URL</button>
    </form>
        <br>
</div>
<?php $__env->stopSection(); ?>
</body>


<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\13600199_short\resources\views/notfound.blade.php ENDPATH**/ ?>