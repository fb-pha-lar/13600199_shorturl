<link href="https://fonts.googleapis.com/css?family=Luckiest+Guy|Mitr|Monoton|Righteous|Yanone+Kaffeesatz&display=swap" rel="stylesheet">

<body style="background-color: whitesmoke; ">

<?php $__env->startSection('content'); ?>
    <div class="container card" style="background-color: white">

    <div class="container" style="background-color: white">
    <br>
    <form method="get" action="<?php echo e(url('/')); ?>" >
        <button type="submit" class="btn btn-lg btn-block" style="background-color: darkslategray; color: white">BACK TO LIST URL</button>
    </form>
    <form method="post" action="<?php echo e(url('/')); ?>" >
        <?php echo csrf_field(); ?>
        <br>
        <div class="card text-center" style="background-color: lightseagreen">
            <div class="card-header">
                <h1  style=" height: 150px; color: white; padding-top: 50px; font-size: 50px; font-family: 'Monoton' ;">SHORT URL</h1>
            </div>
        </div>


        <div class="form-group" style="font-family: 'Yanone Kaffeesatz'">
            <br>
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <p for="exampleInputEmail1" class="input-group-text " style="background-color: lightseagreen; color: white">Input URL</p>
                </div>
                <input type="text" class="form-control"  name="long"  placeholder="Enter URL">
            </div>
            <hr class="my-4">
            <button type="submit" class="btn btn-info btn-lg btn-block">CREATE SHORT URL</button>
            <br>


        </div>
    </form>
    </div>

</div>
<?php $__env->stopSection(); ?>

</body>



<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\13600199_short\resources\views/new.blade.php ENDPATH**/ ?>