<link href="https://fonts.googleapis.com/css?family=Luckiest+Guy|Mitr|Monoton|Righteous|Yanone+Kaffeesatz&display=swap" rel="stylesheet">
<body style="background-color: whitesmoke; ">

<?php $__env->startSection('content'); ?>
<div class="container card" style="background-color: white">

    <br>
    <form method="get" action="<?php echo e(url('/new')); ?>">
        <button type="submit" class="btn btn-lg btn-block" style="background-color: darkorchid; color: white">GO TO SHORT URL</button>
    </form>
    <form>
        <br>
        <div class="card text-center" style="background-color: indigo">
            <div class="card-header">
                <h1  style=" height: 150px; color: white; padding-top: 50px; font-size: 50px; font-family: 'Monoton' ;">LIST URL</h1>
            </div>
        </div>
        <br>
        <?php if(count($shortens)>0): ?>
            <?php $__currentLoopData = $shortens; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $shorten): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                <div style="font-family: 'Righteous'">
                    <a href="<?php echo e(url($shorten -> long)); ?>">
                        <h3 class="text" style="color: darkslateblue"><?php echo e($shorten -> long); ?> </h3>
                    </a>
                    <p>DATE : <?php echo e($shorten -> created_at); ?></p>
                    <div class="input-group mb-3" style="font-family: 'Yanone Kaffeesatz'">
                        <div class="input-group-prepend">
                            <p class="input-group-text" style="background-color: darkslateblue; color: white">View : <?php echo e($shorten -> view); ?></p>
                        </div>
                        <input id="shorturl<?php echo e($shorten -> id); ?>" class="form-control" type="text" style="background-color: white" value="http://www.short.local/t/<?php echo e($shorten -> short); ?>" readonly>
                        <div class="input-group-prepend">
                            <button onclick="copy(this)" id="copyBtn" value="<?php echo e($shorten -> id); ?>" type="button" class="btn" style="background-color: slateblue; color: white">COPY</button>
                        </div>
                    </div>
                    <hr class="my-4">
                </div>
            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
        <?php endif; ?>
    </form>

</div>
<?php $__env->stopSection(); ?>


<script>
    function copy(clickedBtn) {
        var id = clickedBtn.value;
        var copyText = document.querySelector('#shorturl'+id)
        copyText.select();
        document.execCommand('copy');
        alert('Copied' + copyText.value);
    }
</script>


</body>



<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\13600199_short\resources\views/index.blade.php ENDPATH**/ ?>